# Conferwith UI

React UI library for **ConferWith** UI elements and layouts

## How to use

Copy this directory to your local computer, then run:

- `npm install && npm run build`

- To make this component available to other projects on your local computer, run `yarn link`.
- Then go to the project where you want to use this package and run `yarn link "cw-ui"`.

Finally, to fix the multiple copies of React bug that shows up with linked React packages:

- navigate to the root of the `cw-ui` package
- run `npm link "../path/to/your/project/node_modules/react"` ex: `../agent-immersion/node_modules/react`

You can now import `cw-ui` as a normal package into other projects installed from npm like so:

```
import { Button } from 'cw-ui'
```

You can bundle this UI library with shared projects

### Development Server

Use _webpack-dev-server_ to preview UI components locally in **index.tsx**
