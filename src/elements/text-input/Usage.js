import React, {useState} from 'react';
import TextInput from '../../ui/text-input/TextInput';
const Home = () => {
    const [message, setMessage] = useState('hello');

    const onSubmitForm = (e) => {
        e.preventDefault();
    };

    const onChangeTextInput = (value) => {
        setMessage(value);
    };

    return (
        <div>
            <form onSubmit={onSubmitForm}>
                <TextInput value={message} onChange={onChangeTextInput} />
                <button type='submit'>submit</button>
            </form>
        </div>
    );
};

export default Home;
