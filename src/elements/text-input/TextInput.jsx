import React from 'react';
import PropTypes from 'prop-types';
import './TextInput.scss';
import {Icon} from '../icon/Icon';

export const TextInput = ({
    type = 'text',
    value,
    onChange,
    onPressIcon,
    placeholder,
    icon,
    hrLine
}) => {
    const _onChangeInput = (e) => {
        value = e.target.value;
        onChange(e.target.value);
    };

    return (
        <div className="input-container">
            <div className='cw-row'>
                <input
                    type={type}
                    id={placeholder}
                    value={value}
                    onChange={_onChangeInput}
                    required="required"
                    title="3 characters minimum"/>
                <Icon
                    onClick={onPressIcon}
                    iconName={icon}
                    style={{fontSize: '0.8em'}}
                />
                <label htmlFor={placeholder}>{placeholder}</label>
            </div>

            {hrLine === 'true' ? <hr className='middle-break'/> : <hr className='end-break'/>}
        </div>
    );
};

TextInput.propTypes = {
    type: PropTypes.oneOf(['text', 'password']),
    value: PropTypes.string,
    icon: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    style: PropTypes.object,
    hrLine: PropTypes.oneOf(['true', 'false'])
};
