import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

export const Button = ({
    type,
    children,
    disabled,
    color,
    onClick,
    style,
    isActive
}) => {
    const _onClickButton = () => {
        if (type === 'submit') return;
        onClick();
    };

    const getClassNames = () => {
        const classNames = ['cw-button'];

        if (disabled) {
            classNames.push('disabled');
        }
        switch (color) {
        case 'primary':
            classNames.push('primary');
            break;
        case 'success':
            classNames.push('success');
            break;
        default:
            break;
        }
        if (isActive) {
            classNames.push('active');
        }
        return classNames.join(' ');
    };

    return (
        <button
            className={getClassNames()}
            type={type}
            style={style}
            onClick={_onClickButton}
            disabled={disabled}
            isActive={isActive}
        >
            {children}
        </button>
    );
};

Button.propTypes = {
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    style: PropTypes.object,
    isActive: PropTypes.bool,
    onClick: PropTypes.func,
    color: PropTypes.string,
    disabled: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};
