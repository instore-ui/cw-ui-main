import React from 'react';
import './TextInputBackground.scss';

export const TextInputBackground = ({children}) => {
    return (
        <div className='round-background'>
            <div className="card">
                <div className="padding">
                    <form>
                        {children}
                    </form>
                </div>
            </div>
        </div>
    );
};
