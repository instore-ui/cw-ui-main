import React, {useState} from 'react';
import PropTypes from 'prop-types';

import './Select.scss';
import {Icon} from '../icon/Icon';

export const Select = ({
    style,
    onChange,
    selectState,
    children,
    disabled
}) => {
    const [state, setState] = useState(selectState);

    const iconStyle = {
        fontSize: '1.2em',
        color: '#000',
        position: 'absolute',
        right: '0.5em',
        top: '0.5em',
        cursor: 'pointer',
        pointerEvents: 'none'
    };

    const _onChangeSelect = (event) => {
        setState(event.target.value);
        onChange(event.target.value);
    };

    return (
        <div className='cw-select'>
            <select
                style={style}
                className={`${disabled ? 'disabled' : ''}`}
                onChange={_onChangeSelect}
                value={state}
                disabled={disabled}
            >
                {children}
            </select>
            <Icon iconName='cwicon-ICON-Arrow_Down' style={iconStyle}></Icon>
        </div>
    );
};

const Option = ({style, value, label, disabled}) => {
    return (
        <option
            style={style}
            className={`${disabled ? 'disabled' : ''}`}
            value={value}
            disabled={disabled}
        >
            {label}
        </option>
    );
};

Select.Option = Option;

Select.propTypes = {
    style: PropTypes.object,
    onChange: PropTypes.func,
    selectState: PropTypes.string,
    disabled: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

Option.propTypes = {
    style: PropTypes.object,
    value: PropTypes.string,
    label: PropTypes.string,
    disabled: PropTypes.bool
};
