import React, {useState} from 'react';
import Select from './Select';

const {Option} = Select;

const menuOptions = [
    {value: '0', label: 'Select'},
    {value: 'aa', label: 'AA'},
    {value: 'bb', label: 'BB'},
    {value: 'cc', label: 'CC'},
    {value: 'dd', label: 'DD'},
    {value: 'ee', label: 'EE'}
];

const Home = () => {
    const onChange = (value) => {
        setSizeValue(value);
    };

    const [sizeValue, setSizeValue] = useState('0');

    const onSubmitForm = (e) => {
        e.preventDefault();
    };

    return (
        <div>
            <form onSubmit={onSubmitForm}>
                <Select
                    style={{width: '150px'}}
                    onChange={onChange}
                    selectState={sizeValue}
                >
                    {menuOptions.map((option, idx) => (
                        <Option key={idx} value={option.value} label={option.label} />
                    ))}
                </Select>
                <button type='submit'>submit</button>
            </form>
        </div>
    );
};

export default Home;
