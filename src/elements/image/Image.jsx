import React, { useState} from 'react';
import PropTypes from 'prop-types';
import './Image.scss';
import InnerImageZoom from 'react-inner-image-zoom';
import {Modal} from "../../layouts/modal/Modal";


export const Image = ({src, alt, style, isZoomable, isAgent}) => {
    const [isZoom, setIsZoom] = useState(false)
    return (
        <div className='cw-image' style={{position: "relative"}}>
            <img src={src} alt={alt} style={style}/>
            {isZoomable && <i onClick={() => setIsZoom(true)}
                              className='cwicon-ICON-Magnifier'
                              style={{position: 'absolute', bottom: '0.5rem', right: '50%'}}/>}

            <Modal isShowing={isZoom} isAgent={isAgent} hide={() => setIsZoom(false)}>
                <InnerImageZoom src={src}  zoomType="hover" zoomScale={3}/>
            </Modal>
        </div>

    );
};

Image.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    style: PropTypes.object
};
