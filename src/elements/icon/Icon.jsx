import React from 'react';
import PropTypes from 'prop-types';

import './Icon.scss';

export const Icon = ({iconName, style, label, onClick}) => {
    const getClassNames = () => {
        const classNames = ['cw-icon'];

        if (onClick) {
            classNames.push('pointer');
        }

        return classNames.join(' ');
    };

    return (
        <div className={getClassNames()} onClick={onClick}>
            <div className='icon-wrapper'>
                <i className={`${iconName}`} style={style} />
                {label && <p>{label}</p>}
            </div>
        </div>
    );
};

Icon.propTypes = {
    iconName: PropTypes.string,
    style: PropTypes.object,
    label: PropTypes.string,
    onClick: PropTypes.func
};
