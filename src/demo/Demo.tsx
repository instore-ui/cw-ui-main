import React, {useState} from 'react';
import {Button} from '../elements/button/Button';
import {Icon} from '../elements/icon/Icon';
import {PanelHeader} from '../layouts/panel-header/PanelHeader';
import {useModal} from '../layouts/modal/useModal';
import {Modal} from '../layouts/modal/Modal';
import {Basket} from '../layouts/basket/Basket';
import {Header} from '../layouts/modal/header/Header';
import {CartBody} from '../layouts/modal/cartBody/CartBody';
import {CartSubHeader} from '../layouts/modal/cartSubHeader/CartSubHeader';
import {Item} from '../layouts/modal/item/Item';
import {ItemTotalPrice} from '../layouts/modal/itemTotalPrice/ItemTotalPrice';
import {ItemBottom} from '../layouts/modal/itemBottom/ItemBottom';


const Demo = () => {
    const {isShowing, toggle} = useModal();

    const [icon2, setIcon2] = useState('cwicon-ICON-Magnifier');

    const itemData = [{
        id: '200',
        imgUrl: 'https://picsum.photos/200/300',
        itemName: 'This is sample product this containe with discription long description',
        itemPrice: 400,
        size: 'L',
        color: 'Blue',
        qty: 2
    }, {
        id: '300',
        imgUrl: 'https://homepages.cae.wisc.edu/~ece533/images/cat.png',
        itemName: 'This is second example product',
        itemPrice: 500,
        size: 'M',
        color: 'Yellow',
        qty: 5
    }];

    const lightData = [{
        id: '300',
        imgUrl: 'https://picsum.photos/200/300',
        itemName: 'This opacicty desciption long description',
        itemPrice: 450,
        size: 'S',
        color: 'Red',
        qty: 10
    }];

    return (
        <div>
            <div className='cw-row c11'>
                <div className=' c3' style={{backgroundColor: '#e3d1d0'}}>
                    3/14 grid
                </div>
                <div className=' c11' style={{backgroundColor: '#c2e2c0'}}>
                    11/14 grid
                </div>
            </div>

            <div className='cw-row'>
                <div className='c4 cs6' style={{backgroundColor: '#c0b4ef'}}>
                    mobile view 6/12 grid and 4/16 grid for others
                </div>
                <div className='c12 cs6' style={{backgroundColor: '#ce9e9e'}}>
                    mobile view 6/12 grid and 12/16 grid for others
                </div>
            </div>
            {/* responsive demo end*/}

            <PanelHeader>
                <p style={{display: 'inline', padding: '0.5em'}}>
                    Bowers & Wilkins Formation Duo Bluetooth Wi-Fi Stereo Speakers
                </p>
                <p style={{fontWeight: 'bold', padding: '0.5em'}}>£250000</p>
                <Button>
                    <Icon
                        iconName='cwicon-ICON-Bag_Dark'
                        style={{
                            fontSize: '1.5em',
                            cursor: 'pointer',
                            position: 'relative',
                            top: '0.1em'
                        }}
                    />
                </Button>
            </PanelHeader>
            <Button onClick={toggle}>Hello</Button>
            <br/>
            <br/>
            <br/>
            <Basket count={4}/>
            {/* Model Start*/}
            <Modal isShowing={isShowing} hide={toggle}>

                {/* type 1 Header*/}
                <Header
                    icon3Name='cwicon-ICON-Close'
                    headerType='type1'
                    iconCloseOnClick={toggle}
                >heading 1
                </Header>

                {/* type 2 header*/}
                <Header
                    headerType='type2'
                    icon1Name='cwicon-ICON-Shooping'
                    icon2Name={icon2}
                    icon3Name='cwicon-ICON-Close'
                    iconCloseOnClick={toggle}
                    iconStarOnClick={() => setIcon2('cwicon-ICON-Play')}
                    iconLockOnClick={() => {}}
                >Heading 2
                </Header>

                <CartBody>
                    <CartSubHeader
                        iconVisible // lock icon visible (default false)
                    >Multiple choice available discuss the customer</CartSubHeader>
                    {itemData.map((item, key) => {
                        return <Item key={key}
                            data={item}
                            editOnClick={() => {
                            }}
                            deleteOnClick={() => {
                            }}
                        />;
                    })}
                    <ItemTotalPrice
                        label='Total Price'
                        price='305'
                    />
                    <ItemTotalPrice
                        label='Discount Price'
                        price='5'
                    />
                    {/* with boarder*/}
                    <ItemTotalPrice
                        label='sum Price'
                        price='500'
                        boarder
                    />
                    <ItemBottom/>
                    {/* with Opacity*/}
                    {lightData.map((item, key) => {
                        return <Item key={key}
                            light // opacity
                            data={item}
                            editOnClick={() => {
                            }}
                            deleteOnClick={() => {
                            }}
                        />;
                    })}
                    <div className='cw-center'>
                        <Button>Add to Cart</Button>
                    </div>
                </CartBody>
            </Modal>
            {/*    Model End*/}
        </div>
    );
};

export default Demo;
