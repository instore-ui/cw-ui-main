import React from 'react';
import {Icon} from '../../elements/icon/Icon';
import PropTypes from 'prop-types';
import './Basket.scss';
import {CSSTransition, SwitchTransition} from 'react-transition-group';


export const Basket = ({count, style, onClick}) => {
    return (
        <div className='cw-shopping-basket' style={style} onClick={onClick}>
            <Icon iconName='cwicon-BASKET' style={{fontSize: '1.5em'}} onClick={onClick}/>
            <SwitchTransition mode="out-in">
                <CSSTransition
                    classNames="cw_shopping_basket_count"
                    addEndListener={(node, done) => {
                        node.addEventListener("transitionend", done, false);
                    }}
                    key={count}
                >
                    <span className='cw_shopping_basket_count_initial'>{count}</span>
                </CSSTransition>
            </SwitchTransition>
        </div>
    );
};

Basket.propTypes = {
    count: PropTypes.number,
    style: PropTypes.object,
    onClick: PropTypes.func
};
