import React from 'react';
import PropTypes from 'prop-types';

import './PanelHeader.scss';

export const PanelHeader = ({children, noOfRow}) => {
    return (
        <div>{noOfRow === '2' ?
            <div className='panel-header-2-col'>{children}</div> :
            <div className='panel-header'>{children}</div>}</div>);
};

PanelHeader.prototype = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    noOfRow: PropTypes.oneOf(['2'])
};
