import React from 'react';
import {Icon} from '../../../elements/icon/Icon';
import './Item.scss';
import PropTypes from 'prop-types';

export const Item = ({
    light,
    editOnClick,
    deleteOnClick,
    data
}) => {
    const strimeWord = (text) => {
    // set max maximum length of the secription
        const maxLength = 40;
        if (text.length > maxLength) {
            const strime = text.substr(0, maxLength - 3);
            return (`${strime}...`);
        } else {
            return (text);
        }
    };

    return (
        <div className={light ? 'light' : 'normal'} style={{paddingLeft: '3px'}}>
            <div className='cw-row boarder-bottom c12'>
                <div className='c2' style={{margin:"auto"}}>
                    <img className='cw-cart-image' src={`${data.imgUrl}`}/>
                </div>
                <div className='c4' style={{overflowWrap: "break-word"}}>
                    <div className='cw-cart-product-text cart-b-text'>
                        {strimeWord(data.productName)}
                    </div>
                    <div className='cw-cart-product-text cart-s-text'>

                        {data.variant && data.variant.options &&
            data.variant.options.map((option, key) =>
                <>{option.name}:{option.value}/</>
            )}
            Qty: {data.qty}

                    </div>
                </div>
                <div className='c2 cw-cart-price-text'>
          £{data.variant && data.variant.price ? data.variant.price.toFixed(2) :
                        data.price.toFixed(2)}
                </div>
                <div className='c2 edit cw-cart-action-icon'>
                    <Icon
                        iconName='cwicon-ICON-Edit'
                        style={{fontSize: '1.1rem', fontWeight: 'bolder', color: 'black'}}
                        onClick={editOnClick}
                    />
                </div>
                <div className='c2 cw-cart-action-icon'>
                    <Icon
                        iconName='cwicon-ICON-Delete'
                        style={{fontSize: '1em', fontWeight: 'bolder'}}
                        onClick={deleteOnClick}
                    />
                </div>
            </div>
        </div>
    );
};

Item.propTypes = {
    light: PropTypes.bool,
    deleteOnClick: PropTypes.func,
    editOnClick: PropTypes.func,
    data: PropTypes.object
};

