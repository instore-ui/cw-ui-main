import React from 'react';
import {Icon} from '../../../elements/icon/Icon';
import './Variant.scss';
import PropTypes from 'prop-types';


export const Variant = ({
    variantName,
    items,
    iconName,
    onClickButton,
    iconCloseOnClick
}) => {
    return (
        <div className='cw-variant c12 cm12 cs12' style={{paddingBottom: '20px'}}>
            <div className='cw-row cw-center variant-header'>
                {variantName}
            </div>

            <div className='cw-center'>

                <div>
                    {items.map((item, key) => <div key={key} className='list'>
                        <button className='button1' onClick={onClickButton}>{item.value}</button>
                    </div>)}
                </div>
            </div>
            <hr/>
            <div className='cw-row cw-center close-icon'>
                <div className='icon' style={{backgroundColor: '#000000', color: '#ffffff'}}>
                    <Icon
                        iconName={iconName}
                        style={{color: '#ffffff', fontSize: '2em'}}
                        onClick={iconCloseOnClick}
                    />
                </div>
            </div>
        </div>
    );
};

Variant.propTypes = {
    variantName: PropTypes.string,
    iconName: PropTypes.string,
    items: PropTypes.array,
    iconCloseOnClick: PropTypes.func,
    onClickButton: PropTypes.func
};
