import React from 'react';
import {Icon} from '../../../elements/icon/Icon';
import './CartSubHeader.scss';
import PropTypes from 'prop-types';

export const CartSubHeader = ({
    iconVisible,
    children
}) => {
    return (
        <div className='cart-sub-header'>
            <div className='cw-row show cw-cart-sub-header__cart-lock cw-center cs12'>
                {iconVisible ? <Icon
                    iconName='cwicon-BASKET'
                    style={{fontSize: '1.5em'}}
                    // onClick={}
                /> : null
                }
            </div>
            <div className='cw-row cw-cart-sub-header__cart-status cart-s-text cw-center cs12'>
                {children}
            </div>
        </div>
    );
};

CartSubHeader.propTypes = {
    iconVisible: PropTypes.bool,
    style: PropTypes.object
};
