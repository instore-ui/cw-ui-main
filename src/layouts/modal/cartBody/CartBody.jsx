import React from 'react';
import './CartBody.scss';
import PropTypes from 'prop-types';

export const CartBody = ({
    children,
    style
}) => {
    return (
        <div className='cw-modal-body' style={style}>

            {children}

        </div>
    );
};

CartBody.propTypes = {
    headerType: PropTypes.string,
    type: PropTypes.oneOf(['text', 'password']),
    value: PropTypes.string,
    iconCloseOnClick: PropTypes.func,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    style: PropTypes.object
};

