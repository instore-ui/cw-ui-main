import React from 'react';
import './ItemBottom.scss';
import PropTypes from 'prop-types';

export const ItemBottom = () => {
    return (
        <div className='cart-footer'>
            <div className='cw-center cart-b-text'>
                ALREADY ADDED FOR CHECKOUT
            </div>
            <div className='cw-center cart-s-text'>
                These product are now at your checkout basket
            </div>
            <div className='cw-center cart-s-text'>
                You can edit them and complete transaction after ending this call
            </div>

        </div>
    );
};

ItemBottom.propTypes = {
    text: PropTypes.string
};

