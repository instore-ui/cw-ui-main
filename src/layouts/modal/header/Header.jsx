import React from 'react';
import {Icon} from '../../../elements/icon/Icon';
import './Header.scss';
import PropTypes from 'prop-types';


export const Header = ({
    headerType,
    icon1Name,
    icon2Name,
    icon3Name,
    children,
    iconCloseOnClick,
    iconStarOnClick,
    iconLockOnClick
}) => {
    if (headerType === 'type1') {
        return (
            <div className='cw-row cw-cart-header cm12 cs12' style={{height:'4.3rem'}}>
                <div className='c1 cm1 cs1'>
                </div>
                <div className='c10 cm10 cs10' style={{textAlign:'center', padding:0}}>
                    <p>
                    <Icon
                        iconName='cwicon-BASKET'
                        style={{fontSize: '1.5em'}}
                    />
                    </p>
                    <p>{children}</p>
                </div>
                <div className='cw-cart-header__header-icon c1 cm1 cs1'>
                    <Icon
                        iconName={icon3Name}
                        style={{fontSize: '1em'}}
                        onClick={iconCloseOnClick}
                    />
                </div>
            </div>
        );
    } else {
        return (
            <div className='cw-row cw-cart-header c12 cm12 cs12'>
                <div className='c1 cm10 cs1'>
                </div>
                <div className=' c10 cm10 cs10'>
                    {children}
                </div>
                <div onClick={iconLockOnClick} className='header-icon c2 cm2 cs2'>
                    <Icon
                        iconName={icon1Name}
                        style={{fontSize: '1em'}}
                    />
                </div>
                <div onClick={iconStarOnClick} className='header-icon c2 cm2 cs2'>
                    <Icon
                        iconName={icon2Name}
                        style={{fontSize: '1em'}}
                    />
                </div>
                <div onClick={iconCloseOnClick} className='header-icon c2 cm2 cs2'>
                    <Icon
                        iconName={icon3Name}
                        style={{fontSize: '1em'}}
                    />
                </div>
            </div>
        );
    }
};

Header.propTypes = {
    icon1Name: PropTypes.string,
    icon2Name: PropTypes.string,
    icon3Name: PropTypes.string,
    headerType: PropTypes.string,
    value: PropTypes.string,
    iconStarOnClick: PropTypes.func,
    iconLockOnClick: PropTypes.func,
    iconCloseOnClick: PropTypes.func,
    style: PropTypes.object
};
