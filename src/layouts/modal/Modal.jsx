import React from 'react';
import ReactDOM from 'react-dom';
import './Modal.scss';


export const Modal = ({isShowing, hide, children, isAgent}) =>
    isShowing ?
        isAgent ?
            ReactDOM.createPortal(
                <React.Fragment>
                    <div className='cw-modal-overlay-agent' onClick={hide}/>
                    <div
                        className='cw-modal-wrapper-agent'
                        aria-modal
                        aria-hidden
                        tabIndex={-1}
                        role='dialog'
                    >
                        <div className='cw-modal cl7 cm9 cs12'>
                            {children}
                        </div>
                    </div>
                </React.Fragment>,
                document.body
            ) :
            ReactDOM.createPortal(
                <React.Fragment>
                    <div className='cw-modal-overlay' onClick={hide}/>
                    <div
                        className='cw-modal-wrapper'
                        aria-modal
                        aria-hidden
                        tabIndex={-1}
                        role='dialog'
                    >
                        <div className='cw-modal cl7 cm9 cs12'>
                            {children}
                        </div>
                    </div>
                </React.Fragment>,
                document.body
            ) :
        null;
