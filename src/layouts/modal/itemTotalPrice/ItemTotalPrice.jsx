import React from 'react';
import './ItemTotalPrice.scss';
import PropTypes from 'prop-types';

export const ItemTotalPrice = ({
    label,
    price,
    boarder
}) => {
    if (boarder) {
        return <div className='item-total-price'>
            <hr/>
            <div>
                <div className='cw-row  c12'>
                    <div className='c8 cart-total-price-text '>
                        {label}  £{price}
                    </div>
                    <div className='c4'/>
                </div>
            </div>
            <hr/>
        </div>;
    } else {
        return (
            <div className='cw-row  '>
                <div className='c11 cart-total-price-label '>
                    {label}
                </div>
                <div className='c3 cart-total-price-text '>
                    £{price}
                </div>
                <div className='c4'>
                </div>
            </div>
        );
    }
};


ItemTotalPrice.propTypes = {
    label: PropTypes.string,
    price: PropTypes.number,
    boarder: PropTypes.bool
};

