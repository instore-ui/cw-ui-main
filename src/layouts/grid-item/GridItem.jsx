import React from 'react';
import PropTypes from 'prop-types';

import './GridItem.scss';

export const GridItem = ({children}) => {
    return <div className='cw-grid-item'>{children}</div>;
};

GridItem.prototype = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};
