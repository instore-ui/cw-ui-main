import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import Demo from './demo/Demo'

ReactDOM.render(<Demo />, document.querySelector('#preview'))
